from django.db import models

class Address(models.Model):
    lat = models.DecimalField(max_digits=20,decimal_places=10)
    lon = models.DecimalField(max_digits=20,decimal_places=10)
    name = models.TextField()
    update_on = models.DateTimeField()
