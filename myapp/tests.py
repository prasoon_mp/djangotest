from django.test import TestCase
from myapp.models import Address
from django.test import Client
import  json
from datetime import datetime, timedelta
class AddressTestCase(TestCase):
    def setUp(self):
        Address.objects.create(lat=-12.1212, lon=12.1212, name="Benguela, Angola", update_on=datetime.now())
        date_5_days_ago = datetime.now() - timedelta(days=5)
        addr=Address.objects.create(lat=-13.1212, lon=13.1212, name="ww", update_on=date_5_days_ago)
        addr.update_on=date_5_days_ago
        addr.save()

    def test_address_new_entry(self):

        response = self.client.get('/myapp/get_address/-34.44076/-58.70521/')
        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content.decode("utf-8"))['name'], "Pedro Eugenio Aramburu, El Triángulo, Partido de Malvinas Argentinas, Bs. As., 1.619, Argentina")

    def test_address_already_exist(self):
        response = self.client.get('/myapp/get_address/-12.1212/12.1212/')
        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content.decode("utf-8"))['name'], "Benguela, Angola")


    def test_address_expiry(self):
        response = self.client.get('/myapp/get_address/-13.1212/13.1212/')
        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content.decode("utf-8"))['name'], "Benguela, Angola")
