from django.urls import path

from . import views

urlpatterns = [
    path('get_address/<lat>/<lon>/', views.getAddress, name='get_address'),
]
