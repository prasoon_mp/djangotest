from django.http import HttpResponse
from myapp.models import Address
from decimal import Decimal
import urllib.request, json
from urllib.request import Request, urlopen
import requests
from datetime import datetime, timedelta

def fetch_address(lat,lon):
    url= 'http://nominatim.openstreetmap.org/reverse?format=json&lat='+lat+'&lon='+lon
    data = json.loads(requests.get(url).text)
    return data
def getAddress(request, lat,lon):
    try:
        addr=Address.objects.get(lat=Decimal(lat),lon=Decimal(lon))
        print (addr.update_on)
        date_5_days_ago = datetime.now() - timedelta(days=5)
        if addr.update_on > date_5_days_ago:
            data = fetch_address(lat,lon)
            addr.name=data['display_name']
            addr.update_on=datetime.now()
            addr.save()
    except Exception as e:
        print (e)
        data = fetch_address(lat,lon)
        addr=Address.objects.create(lat=Decimal(lat), lon=Decimal(lon), name=data['display_name'],update_on=datetime.now())
    return HttpResponse(json.dumps({'name':addr.name}), content_type="application/json")
